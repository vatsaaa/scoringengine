"""
This is the jobs module and supports all the ReST actions for the JOBLIST collection
"""

# System modules
from datetime import datetime

# 3rd party modules
from flask import make_response, abort


def get_timestamp():
    return datetime.now().strftime(("%Y-%m-%d %H:%M:%S"))


# Data to serve with our API
JOBLIST = {
    "Farrell": {
        "fname": "Doug",
        "lname": "Farrell",
        "timestamp": get_timestamp(),
    },
    "Brockman": {
        "fname": "Kent",
        "lname": "Brockman",
        "timestamp": get_timestamp(),
    },
    "Easter": {
        "fname": "Bunny",
        "lname": "Easter",
        "timestamp": get_timestamp(),
    },
}


def list():
    """
    This function responds to a request for /api/jobs
    with the complete lists of jobs

    :return:        json string of list of jobs
    """
    # Create the list of jobs from our data
    return [JOBLIST[key] for key in sorted(JOBLIST.keys())]


def get(lname):
    """
    This function responds to a request for /api/jobs/{lname}
    with one matching job from jobs

    :param lname:   last name of job to find
    :return:        job matching last name
    """
    # Does the job exist in jobs?
    if lname in JOBLIST:
        job = JOBLIST.get(lname)

    # otherwise, nope, not found
    else:
        abort(
            404, "Job with last name {lname} not found".format(lname=lname)
        )

    return job


def create(job):
    """
    This function creates a new job in the jobs structure
    based on the passed in job data

    :param job:  job to create in jobs structure
    :return:        201 on success, 406 on job exists
    """
    lname = job.get("lname", None)
    fname = job.get("fname", None)

    # Does the job exist already?
    if lname not in JOBLIST and lname is not None:
        JOBLIST[lname] = {
            "lname": lname,
            "fname": fname,
            "timestamp": get_timestamp(),
        }
        return make_response(
            "{lname} successfully created".format(lname=lname), 201
        )

    # Otherwise, they exist, that's an error
    else:
        abort(
            406,
            "Job with last name {lname} already exists".format(lname=lname),
        )


def update(lname, job):
    """
    This function updates an existing job in the jobs structure

    :param lname:   last name of job to update in the jobs structure
    :param job:  job to update
    :return:        updated job structure
    """
    # Does the job exist in jobs?
    if lname in JOBLIST:
        JOBLIST[lname]["fname"] = job.get("fname")
        JOBLIST[lname]["timestamp"] = get_timestamp()

        return JOBLIST[lname]

    # otherwise, nope, that's an error
    else:
        abort(
            404, "Job with last name {lname} not found".format(lname=lname)
        )


def delete(lname):
    """
    This function deletes a job from the jobs structure

    :param lname:   last name of job to delete
    :return:        200 on successful delete, 404 if not found
    """
    # Does the job to delete exist?
    if lname in JOBLIST:
        del JOBLIST[lname]
        return make_response(
            "{lname} successfully deleted".format(lname=lname), 200
        )

    # Otherwise, nope, job to delete not found
    else:
        abort(
            404, "Job with last name {lname} not found".format(lname=lname)
        )
