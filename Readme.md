# JD2CV mapping
## Project Structure
### Features Extraction
Celery and RabbitMQ based
* JD: Extracts features from JD, publishes xml/json as selected by the user and saves it into a NoSQL DB
* CV: Extracts features from CV, publishes xml/json as selected by the user and saves it into a NoSQL DB

### Scoring Engine
Has the following endpoints
* jd: JDs xml is sent and mapping CVs are found from it
    * xml: reads xml, converts it to json, saves it in NoSQL DB & passes it onto the json scoring endpoint
    * json: reads json & scores
* cv: CVs xml is sent and mapping JDs are found from it
    * xml
    * json
    
    Mapping score is based on the following
        JD to CV --> TF/IDF score based on Original documents
        JDXML to CVXML --> TF/IDF score based on 2 xml documents
        JD Features to CV Features matched 1/1 and scored
        The macthing may not be all and perfect and hence weighted scores needs to be done
        e.g. 1/1 skills matching has highest weight, xml/xml has lower, doc/doc has lowest
        1/1 skills match - tech-skills highest weight, domain skills next, soft-skills next
        However, weights need to remain configurable as different jobs and users would want these to be different
    
## Project Utilities
* [XML to JSON Converter](http://www.utilities-online.info/xmltojson/#.XgT6F9YzYWo)

## Project Setup
### Required
* Python 3.6 with virtualenv
* [RabbitMQ](https://www.rabbitmq.com/)
* [Celery](http://www.celeryproject.org/)



https://www.patricksoftwareblog.com/unit-testing-a-flask-application/

http://www.blog.pythonlibrary.org/2016/07/07/python-3-testing-an-intro-to-unittest/

https://realpython.com/python-testing/#writing-your-first-test

https://realpython.com/python-testing/

https://www.fullstackpython.com/api-creation.html

https://apievangelist.com/2018/02/03/api-is-not-just-rest/

https://www.freecodecamp.org/news/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563/

https://www.freecodecamp.org/news/how-to-use-python-and-flask-to-build-a-web-app-an-in-depth-tutorial-437dbfe9f1c6/

https://hackernoon.com/several-social-engineering-tricks-bf1f3x1z

[2 step resume info extraction](https://www.hindawi.com/journals/mpe/2018/5761287/)

https://spacy.io/usage/training

https://spacy.io/usage/examples

https://dzone.com/articles/cv-r-cvs-retrieval-system-based-on-job-description

https://www.kdnuggets.com/2017/05/deep-learning-extract-knowledge-job-descriptions.html

https://www.linkedin.com/pulse/how-easy-scraping-data-from-linkedin-profiles-david-craven/

[Scrape LinkedIn](https://github.com/austinoboyle/scrape-linkedin-selenium)

https://www.omkarpathak.in/2018/12/18/writing-your-own-resume-parser/
https://github.com/OmkarPathak/ResumeParser

https://towardsdatascience.com/do-the-keywords-in-your-resume-aptly-represent-what-type-of-data-scientist-you-are-59134105ba0d

https://github.com/DataTurks-Engg/Entity-Recognition-In-Resumes-SpaCy

https://dev.to/usamaashraf/microservices--rabbitmq-on-docker-e2f
https://tests4geeks.com/python-celery-rabbitmq-tutorial/
https://www.distributedpython.com/

https://www.freecodecamp.org/news/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563/
https://flask.palletsprojects.com/en/1.1.x/patterns/appfactories/

