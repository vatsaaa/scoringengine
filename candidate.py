"""
This is the candidate module and supports all the REST actions for the CANDIDATELIST collection
"""

# System modules
from datetime import datetime

# 3rd party modules
from flask import make_response, abort


def get_timestamp():
    return datetime.now().strftime(("%Y-%m-%d %H:%M:%S"))


# Data to serve with our API
CANDIDATELIST = {
    "Farrell": {
        "fname": "Doug",
        "lname": "Farrell",
        "timestamp": get_timestamp(),
    },
    "Brockman": {
        "fname": "Kent",
        "lname": "Brockman",
        "timestamp": get_timestamp(),
    },
    "Easter": {
        "fname": "Bunny",
        "lname": "Easter",
        "timestamp": get_timestamp(),
    },
}


def list():
    """
    This function responds to a request for /api/candidate
    with the complete lists of candidate

    :return:        json string of list of candidate
    """
    # Create candidate list from our data
    return [CANDIDATELIST[key] for key in sorted(CANDIDATELIST.keys())]


def get(lname):
    """
    This function responds to a request for /api/candidate/{lname}
    with one matching candidate from candidate list

    :param lname:   last name of candidate to find
    :return:        candidate matching last name
    """
    # Does the candidate exist in candidate list?
    if lname in CANDIDATELIST:
        candidate = CANDIDATELIST.get(lname)

    # otherwise, nope, not found
    else:
        abort(
            404, "Candidate with last name {lname} not found".format(lname=lname)
        )

    return candidate


def create(candidate):
    """
    This function creates a new candidate in the candidate list structure
    based on the passed in candidate data

    :param candidate:  candidate to create in candidate list structure
    :return:        201 on success, 406 on candidate exists
    """
    lname = candidate.get("lname", None)
    fname = candidate.get("fname", None)

    # Does the candidate exist already?
    if lname not in CANDIDATELIST and lname is not None:
        CANDIDATELIST[lname] = {
            "lname": lname,
            "fname": fname,
            "timestamp": get_timestamp(),
        }
        return make_response(
            "{lname} successfully created".format(lname=lname), 201
        )

    # Otherwise, they exist, that's an error
    else:
        abort(
            406,
            "Candidate with last name {lname} already exists".format(lname=lname),
        )


def update(lname, candidate):
    """
    This function updates an existing candidate in the candidate list structure

    :param lname:   last name of candidate to update in the candidate list structure
    :param candidate:  candidate to update
    :return:        updated candidate list structure
    """
    # Does the candidate exist in candidate list?
    if lname in CANDIDATELIST:
        CANDIDATELIST[lname]["fname"] = candidate.get("fname")
        CANDIDATELIST[lname]["timestamp"] = get_timestamp()

        return CANDIDATELIST[lname]

    # otherwise, nope, that's an error
    else:
        abort(
            404, "Candidate with last name {lname} not found".format(lname=lname)
        )


def delete(lname):
    """
    This function deletes a candidate from the candidate list structure

    :param lname:   last name of candidate to delete
    :return:        200 on successful delete, 404 if not found
    """
    # Does the candidate to delete exist?
    if lname in CANDIDATELIST:
        del CANDIDATELIST[lname]
        return make_response(
            "{lname} successfully deleted".format(lname=lname), 200
        )

    # Otherwise, nope, candidate to delete not found
    else:
        abort(
            404, "Candidate with last name {lname} not found".format(lname=lname)
        )
